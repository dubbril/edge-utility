# Enable This Properties For Realtime Edit Html On Develop
```
spring.thymeleaf.cache=false
spring.thymeleaf.prefix=file:src/main/resources/templates/
spring.web.resources.static-locations=file:src/main/resources/static/
spring.web.resources.cache.period=0
```

# Creat package.jon
```
npm init -y
```

# Command For Install Package Node JS
```
npm install -D tailwindcss
```

# Create tailwind.config.json
```
npx tailwindcss init
```
- And update tailwind.config.json on this line
    ```
    content: [],
    ```
-   To
    ```
    content: [
        "./src/main/resources/templates/**/*.html"
    ],
    ```
    
# Create input.css in "/resources/static/"
- input.css
```
@tailwind base;
@tailwind components;
@tailwind utilities;
```

# Command For Build Tailwind Css Realtime On Develop
```
npx tailwindcss -i ./src/main/resources/static/input.css -o ./src/main/resources/static/style.css --watch
```
