package org.dubbril.learn.edge_utility.config;

import jakarta.annotation.PostConstruct;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.BouncyGPG;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.callbacks.KeyringConfigCallbacks;
import name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.InMemoryKeyring;
import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.operator.jcajce.JcaKeyFingerprintCalculator;
import org.bouncycastle.util.io.Streams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static name.neuhalfen.projects.crypto.bouncycastle.openpgp.keys.keyrings.KeyringConfigs.forGpgExportedKeys;

@Slf4j
@Component
public class PgpFileEncryption {
    private static final int BUFF_SIZE = 8 * 1024;
    private InMemoryKeyring inMemoryKeyring;
    private final EdgeConfigPath edgeConfigPath;

    @Autowired
    public PgpFileEncryption(EdgeConfigPath edgeConfigPath) {
        this.edgeConfigPath = edgeConfigPath;
    }

    @SneakyThrows
    @PostConstruct
    private void setupKeyring() {
        addPrivateKey();
        addPublicKey();

        PGPSecretKeyRingCollection secretKeyRings = this.inMemoryKeyring.getSecretKeyRings();
        Set<PGPPublicKey> pgpPublicKeys = extractPublicKeyFromPrivateKey(secretKeyRings);
        for (PGPPublicKey pgpPublicKey : pgpPublicKeys) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            pgpPublicKey.encode(outputStream);
            inMemoryKeyring.addPublicKey(outputStream.toByteArray());
        }
    }

    @SneakyThrows
    private void addPrivateKey() {
        Map<Long, char[]> keyIdToPassphraseMap = new HashMap<>();
        Map<String, String> mapPgpPrivateKey = edgeConfigPath.getMapPgpPrivateKey();
        List<byte[]> privateByte = new ArrayList<>();

        for (Map.Entry<String, String> entry : mapPgpPrivateKey.entrySet()) {
            byte[] bytes = Files.readAllBytes(Paths.get(entry.getKey()));
            privateByte.add(bytes);

            Set<Long> keyIds = extractKeyIDFromPrivateKey(bytes);
            for (Long keyId : keyIds) {
                keyIdToPassphraseMap.put(keyId, entry.getValue().toCharArray());
            }
        }

        this.inMemoryKeyring = forGpgExportedKeys(KeyringConfigCallbacks.withPasswordsFromMap(keyIdToPassphraseMap));
        for (byte[] privateItem : privateByte) {
            this.inMemoryKeyring.addSecretKey(privateItem);
        }
    }

    @SneakyThrows
    private void addPublicKey() {
        Map<String, String> mapPgpPublicKey = edgeConfigPath.getMapPgpPublicKey();
        for (Map.Entry<String, String> entry : mapPgpPublicKey.entrySet()) {
            byte[] bytes = Files.readAllBytes(Paths.get(entry.getValue()));
            this.inMemoryKeyring.addPublicKey(bytes);
        }
    }

    @SneakyThrows
    private Set<PGPPublicKey> extractPublicKeyFromPrivateKey(PGPSecretKeyRingCollection secKeyRings) {
        Set<PGPPublicKey> publicKeys = new HashSet<>();

        for (PGPSecretKeyRing keyRing : secKeyRings) {
            for (PGPSecretKey key : keyRing) {
                if (key != null && key.getPublicKey().isMasterKey()) {
                    publicKeys.add(key.getPublicKey());
                }
            }
        }

        if (secKeyRings.size() != publicKeys.size()) {
            throw new PGPException("Some private key is incorrect");
        }

        return publicKeys;
    }

    @SneakyThrows
    public static Set<Long> extractKeyIDFromPrivateKey(byte[] keyBytes) {
        InputStream keyIn = new ByteArrayInputStream(keyBytes);
        ArmoredInputStream armoredInputStream = new ArmoredInputStream(keyIn);
        PGPSecretKeyRingCollection secretKeyRingCollection = new PGPSecretKeyRingCollection(armoredInputStream, new JcaKeyFingerprintCalculator());

        Set<Long> keyIds = new HashSet<>();
        for (PGPSecretKeyRing keyRing : secretKeyRingCollection) {
            for (PGPSecretKey key : keyRing) {
                keyIds.add(key.getKeyID());
            }
        }

        if (keyIds.isEmpty()) {
            throw new IllegalArgumentException("No secret key found in file");
        }
        return keyIds;
    }

    @SneakyThrows
    public void encryptPgpSign(String recipientEmail, String userIdSign, String sourceFilePath, String destinationFilePath) {

        BouncyGPG.registerProvider();
        Path srcPath = Paths.get(sourceFilePath);
        Path destPath = Paths.get(destinationFilePath);

        try (
                final OutputStream fileOutput = Files.newOutputStream(destPath);
                final BufferedOutputStream bufferedOutput = new BufferedOutputStream(fileOutput, BUFF_SIZE);
                final OutputStream outputStream = BouncyGPG.encryptToStream()
                        .withConfig(this.inMemoryKeyring)
                        .withDefaultAlgorithms()
                        .toRecipient(recipientEmail)
                        .andSignWith(userIdSign)
                        .armorAsciiOutput()
                        .andWriteTo(bufferedOutput);

                final InputStream inputStream = Files.newInputStream(srcPath)
        ) {
            Streams.pipeAll(inputStream, outputStream);
            log.info("File is encrypted");
        }
    }

    @SneakyThrows
    public void decryptPgpVerify(String userIdSign, String sourceFilePath, String destinationFilePath) {

        BouncyGPG.registerProvider();
        Path srcPath = Paths.get(sourceFilePath);
        Path destPath = Paths.get(destinationFilePath);

        try (
                final InputStream fileInputStream = Files.newInputStream(srcPath);
                final OutputStream fileOutputStream = Files.newOutputStream(destPath);
                final BufferedOutputStream bufferedOutput = new BufferedOutputStream(fileOutputStream, BUFF_SIZE);

                final InputStream outputStream = BouncyGPG.decryptAndVerifyStream()
                        .withConfig(this.inMemoryKeyring)
                        .andRequireSignatureFromAllKeys(userIdSign)
                        .fromEncryptedInputStream(fileInputStream);

        ) {
            Streams.pipeAll(outputStream, bufferedOutput);
            log.info("File is decrypted");
        }

    }

}
