package org.dubbril.learn.edge_utility.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Getter
@Configuration
public class EdgeConfigPath {

    @Value("${edge-edw-private-user-id}")
    private String edgeEdwPrivateUserId;

    @Value("${edge-edw-public-user-id}")
    private String edgeEdwPublicUserId;

    @Value("${edge-debt-private-user-id}")
    private String edgeDebtPrivateUserId;

    @Value("${edge-debt-public-user-id}")
    private String edgeDebtPublicUserId;

    @Value("#{${map.pgp.private.key}}")
    private Map<String, String> mapPgpPrivateKey;

    @Value("#{${map.pgp.pubic.key}}")
    private Map<String, String> mapPgpPublicKey;

}
