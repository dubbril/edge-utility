package org.dubbril.learn.edge_utility.controller;

import org.dubbril.learn.edge_utility.service.SpecialistService;
import org.dubbril.learn.edge_utility.utils.Utility;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;

@Validated
@RestController
@RequestMapping("/api/v1/cryptography/specialist")
public class SpecialistController {
    private final SpecialistService specialistService;

    public SpecialistController(SpecialistService specialistService) {
        this.specialistService = specialistService;
    }

    @PostMapping(value = "make", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> makeSpecialist(@RequestParam("file") MultipartFile file,
                                                 @RequestParam("index") int index) {
        String strResult = specialistService.makeSpecialist(file, index);
        return ResponseEntity.ok().contentType(MediaType.TEXT_PLAIN).body(strResult);
    }

    @GetMapping("/download")
    public ResponseEntity<byte[]> downloadFile() {
        byte[] bytes = specialistService.downloadExampleFile();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "Add_Special_list_Datamart.xlsx");
        headers.setContentLength(bytes.length);
        return ResponseEntity.ok().headers(headers).body(bytes);
    }

    @PostMapping(value = "delete", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> makeSpecialist(
            @RequestParam("file") MultipartFile file, @RequestParam("deleteId") Set<String> deleteId) {
        String fileName = "EIM_EDGE_BLACKLIST_" + Utility.dateWithYearMonthDay() + ".txt";
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        ByteArrayResource resource = specialistService.readData(file, deleteId);
        return ResponseEntity.ok().headers(headers).body(resource);
    }
}
