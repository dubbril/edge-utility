package org.dubbril.learn.edge_utility.controller;

import org.dubbril.learn.edge_utility.service.PgpEncryptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/v1/pgp")
public class PgpEncryptController {

    private final PgpEncryptService pgpEncryptService;

    @Autowired
    public PgpEncryptController(PgpEncryptService pgpEncryptService) {
        this.pgpEncryptService = pgpEncryptService;
    }

    @GetMapping(value = "encrypt-sign")
    public ResponseEntity<String> encryptGpgSign() {
        String response = pgpEncryptService.gpgEncryptSign();
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "decrypt-verify")
    public ResponseEntity<String> decryptGpgSign() {
        String response = pgpEncryptService.gpgDecryptVerify();
        return ResponseEntity.ok(response);
    }

}
