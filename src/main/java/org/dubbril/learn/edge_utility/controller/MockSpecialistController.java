package org.dubbril.learn.edge_utility.controller;

import org.dubbril.learn.edge_utility.service.MockSpecialistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/v1/cryptography/mock")
public class MockSpecialistController {

    private final MockSpecialistService mockSpecialistService;

    @Autowired
    public MockSpecialistController(MockSpecialistService mockSpecialistService) {
        this.mockSpecialistService = mockSpecialistService;
    }

    @GetMapping(value = "make/random")
    public ResponseEntity<byte[]> mockSpecialist(@RequestParam String group, @RequestParam int size) {
        String strResult = mockSpecialistService.mockSpecialist(group, size);
        byte[] fileBytes = strResult.getBytes();

        HttpHeaders headers = new HttpHeaders();
        String fileName = "specialist_" + group + "_" + size + ".txt";
        headers.add("Content-Disposition", "attachment; filename=" + fileName);

        return new ResponseEntity<>(fileBytes, headers, HttpStatus.OK);

    }

}
