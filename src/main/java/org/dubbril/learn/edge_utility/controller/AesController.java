package org.dubbril.learn.edge_utility.controller;

import jakarta.validation.Valid;
import org.dubbril.learn.edge_utility.domain.AesRequest;
import org.dubbril.learn.edge_utility.service.AesCryptographyService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/v1/cryptography/aes")
public class AesController {
    private final AesCryptographyService aesCryptographyService;

    public AesController(AesCryptographyService aesCryptographyService) {
        this.aesCryptographyService = aesCryptographyService;
    }

    @PostMapping(value = "decrypt", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> decrypted(@RequestBody @Valid AesRequest aesRequest) {
        String encrypted = aesCryptographyService.decryptAes(aesRequest);
        return ResponseEntity.ok(encrypted);
    }

    @PostMapping(value = "encrypt", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> encrypted(@RequestBody @Valid AesRequest aesRequest) {
        String encrypted = aesCryptographyService.encryptData(aesRequest);
        return ResponseEntity.ok(encrypted);
    }
}
