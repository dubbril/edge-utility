package org.dubbril.learn.edge_utility.controller;

import org.dubbril.learn.edge_utility.domain.AesRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("aesRequest", new AesRequest());
        return "index";
    }
}
