package org.dubbril.learn.edge_utility.controller;

import org.dubbril.learn.edge_utility.service.JsonStringConvertService;
import org.dubbril.learn.edge_utility.utils.ImageTypeChecker;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequestMapping("/api/v1/cryptography/json")
public class JsonStringConvertController {
    private final JsonStringConvertService jsonStringConvertService;

    public JsonStringConvertController(JsonStringConvertService jsonStringConvertService) {
        this.jsonStringConvertService = jsonStringConvertService;
    }

    @PostMapping(value = "escaped", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> decrypted(@RequestBody String strJson) {
        String jsonEscaped = jsonStringConvertService.convertStringToJsonEscaped(strJson);
        return ResponseEntity.ok(jsonEscaped);
    }

    @PostMapping(value = "base64/image", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> base64ToImage(@RequestBody String base64String) {
        byte[] bytes = jsonStringConvertService.convertBase46ToImage(base64String);
        MediaType mediaType = ImageTypeChecker.checkImageType(bytes);
        return ResponseEntity.ok().contentType(mediaType).body(bytes);
    }

    @PostMapping(value = "image/base64")
    public ResponseEntity<String> handleBinaryRequest(@RequestBody byte[] requestBody) {
        String base64String = jsonStringConvertService.convertImageToBase64(requestBody);
        return ResponseEntity.ok(base64String);
    }

    @PostMapping(value = "snake-to-camel", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> snakeToCamel(@RequestBody Object object) {
        Object result = jsonStringConvertService.convertJsonSnakeToCamel(object);
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "camel-to-snake", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> camelToSnake(@RequestBody Object object) {
        Object result = jsonStringConvertService.convertJsonCamelToSnake(object);
        return ResponseEntity.ok(result);
    }
}
