package org.dubbril.learn.edge_utility.service;


import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.dubbril.learn.edge_utility.domain.data.PeopleFullName;
import org.dubbril.learn.edge_utility.utils.Utility;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Slf4j
@Service
public class MockSpecialistService {

    @SneakyThrows
    private List<PeopleFullName> readDataSet() {
        File peopleFullNameData = new ClassPathResource("data-set/people_full_name.txt").getFile();
        return Files.readAllLines(Paths.get(peopleFullNameData.getAbsolutePath()))
                .stream().skip(1).map(PeopleFullName::new).toList();
    }

    private List<PeopleFullName> filterDataGroup(String group) {
        List<PeopleFullName> peopleDataSet = readDataSet();

        if ("th".equalsIgnoreCase(group)) {
            return peopleDataSet.stream().filter(p -> p.getNationality().equalsIgnoreCase("th")).toList();
        }

        if ("en".equalsIgnoreCase(group)) {
            return peopleDataSet.stream().filter(p -> p.getNationality().equalsIgnoreCase("en")).toList();
        }

        return peopleDataSet;
    }

    // Max size of th group = 1,185,173
    public String mockSpecialist(String group, int size) {
        List<PeopleFullName> peopleDataSet = filterDataGroup(group);

        var result = new StringBuilder("row_no|CustomerNo|Customer_Name_Th|ChkThaiName|ChkEngName|Customer_Name_En|DOB|CountryCode|CustomerType|Zipcode|ACTION|Old_Act|OVRACT|POB|ReasonCode|RTN_Customer|SrcSEQ");
        for (int i = 1; i <= size; i++) {
            var customerName = peopleDataSet.get(i - 1).getFullName();
            var customerId = Utility.generateRandomThaiIDCard();
            var specialCode = RandomStringUtils.randomAlphabetic(2).toUpperCase();
            var strResult =
                    "\n" + i + "|" + customerId + "|" + customerName + "|" + customerName + "||||TH|ZZ|0|R|B|Y||"
                            + specialCode + "|"
                            + customerId + "|(1)(2)(3)";
            result.append(strResult);
        }
        return result.toString();
    }
}
