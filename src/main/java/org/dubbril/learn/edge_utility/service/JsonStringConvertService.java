package org.dubbril.learn.edge_utility.service;

import lombok.SneakyThrows;
import org.apache.commons.text.StringEscapeUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.dubbril.learn.edge_utility.utils.JsonConvert;
import org.springframework.stereotype.Service;

import java.util.Base64;

import static org.dubbril.learn.edge_utility.utils.JsonCase.CAMEL_CASE_TO_SNAKE_CASE;
import static org.dubbril.learn.edge_utility.utils.JsonCase.SNAKE_CASE_TO_CAMEL_CASE;

@Service
public class JsonStringConvertService {

    @SneakyThrows
    public String convertStringToJsonEscaped(String strJson) {
        Object jsonObject = convertToJson(strJson);
        return StringEscapeUtils.escapeJson(jsonObject.toString());
    }

    @SneakyThrows
    private static Object convertToJson(String str) {
        try {
            return new JSONObject(str);
        } catch (JSONException jsonException) {
            return new JSONArray(str);
        }
    }

    public byte[] convertBase46ToImage(String base64String) {
        return Base64.getDecoder().decode(base64String);
    }

    public String convertImageToBase64(byte[] imageByteArray) {
        return Base64.getEncoder().encodeToString(imageByteArray);
    }

    public Object convertJsonCamelToSnake(Object object) {
        return JsonConvert.convertTo(object, CAMEL_CASE_TO_SNAKE_CASE);
    }

    public Object convertJsonSnakeToCamel(Object object) {
        return JsonConvert.convertTo(object, SNAKE_CASE_TO_CAMEL_CASE);
    }
}
