package org.dubbril.learn.edge_utility.service;

import lombok.SneakyThrows;
import org.dubbril.learn.edge_utility.domain.AesRequest;
import org.dubbril.learn.edge_utility.utils.AESEncrypt;
import org.dubbril.learn.edge_utility.utils.Utility;
import org.springframework.stereotype.Service;

@Service
public class AesCryptographyService {
    private final AESEncrypt aesEncrypt;

    public AesCryptographyService(AESEncrypt aesEncrypt) {
        this.aesEncrypt = aesEncrypt;
    }

    @SneakyThrows
    public String decryptAes(AesRequest aesRequest) {
        if (Utility.isHexString(aesRequest.getKey())) {
            return aesEncrypt.decryptWithHexKey(aesRequest.getData(), aesRequest.getKey());
        }
        return aesEncrypt.decryptWithBase64Key(aesRequest.getData(), aesRequest.getKey());
    }

    @SneakyThrows
    public String encryptData(AesRequest aesRequest) {
        if (Utility.isHexString(aesRequest.getKey())) {
            return aesEncrypt.encryptWithHexKey(aesRequest.getData(), aesRequest.getKey());
        }
        return aesEncrypt.encryptWithBase64Key(aesRequest.getData(), aesRequest.getKey());
    }

}
