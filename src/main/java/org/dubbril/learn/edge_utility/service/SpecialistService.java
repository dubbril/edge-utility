package org.dubbril.learn.edge_utility.service;

import lombok.SneakyThrows;
import org.apache.poi.ss.usermodel.*;
import org.dubbril.learn.edge_utility.domain.SpecialistRequest;
import org.dubbril.learn.edge_utility.exception.CustomException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.DecimalFormat;
import java.util.*;

/**
 * @author Sompong Thongmee
 * @create 13/04/2023 23:06
 * @project edge-cryptography
 */

@Service
public class SpecialistService {

    @SneakyThrows
    private static Map<Integer, List<String>> getDataFromExcel(MultipartFile requestBody) {
        try (Workbook book = WorkbookFactory.create(requestBody.getInputStream())) {
            Sheet sheet = book.getSheetAt(0);
            var df = new DecimalFormat("#");
            Map<Integer, List<String>> data = new HashMap<>();

            for (int i = 1; i <= sheet.getLastRowNum(); i++) {
                List<String> txtList = new ArrayList<>();
                for (Cell cell : sheet.getRow(i)) {
                    CellType cellType = cell.getCellType();
                    switch (cellType) {
                        case NUMERIC -> txtList.add(df.format(cell.getNumericCellValue()));
                        case STRING -> txtList.add(cell.getRichStringCellValue().getString().trim());
                        default -> throw new CustomException("Cell Type Not Support");
                    }
                }
                data.put(i, txtList);
            }
            return data;
        } catch (Exception ex) {
            throw new CustomException(ex);
        }
    }

    @SneakyThrows
    public byte[] downloadExampleFile() {
        ClassPathResource resource = new ClassPathResource("example/Add_Special_list_Datamart.xlsx");
        try (InputStream inputStream = resource.getInputStream()) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int bytesRead;
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                byteArrayOutputStream.write(buffer, 0, bytesRead);
            }
            return byteArrayOutputStream.toByteArray();
        }
    }

    public ByteArrayResource readData(MultipartFile file, Set<String> deleteId) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            int index = 1;
            String dataLine;
            String header = reader.readLine();
            StringBuilder sb = new StringBuilder(header);
            while ((dataLine = reader.readLine()) != null) {
                SpecialistRequest specialist = new SpecialistRequest(dataLine);
                if (deleteId.contains(specialist.getCustomerNo())) {
                    continue;
                }
                specialist.setRowNo(String.valueOf(index++));
                sb.append(System.lineSeparator());
                sb.append(specialist);
            }
            return new ByteArrayResource(sb.toString().getBytes());
        } catch (IOException e) {
            throw new CustomException(e);
        }
    }

    public String makeSpecialist(MultipartFile requestBody, int index) {
        Map<Integer, List<String>> dataFromExcel = getDataFromExcel(requestBody);
        var result = new StringBuilder();
        for (var dataItem : dataFromExcel.entrySet()) {
            List<String> value = dataItem.getValue();
            var customerId = value.get(1);
            var customerName = value.get(2);
            var specialCode = value.get(3).substring(value.get(3).length() - 2);
            var strResult =
                    index++ + "|" + customerId + "|" + customerName + "|" + customerName + "||||TH|ZZ|0|R|B|Y||"
                            + specialCode + "|"
                            + customerId + "|(1)(2)(3)" + System.lineSeparator();
            result.append(strResult);
        }
        return result.toString().trim();
    }
}
