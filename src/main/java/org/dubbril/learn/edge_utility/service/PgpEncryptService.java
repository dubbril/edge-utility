package org.dubbril.learn.edge_utility.service;


import lombok.extern.slf4j.Slf4j;
import org.dubbril.learn.edge_utility.config.EdgeConfigPath;
import org.dubbril.learn.edge_utility.config.PgpFileEncryption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;

@Slf4j
@Service
public class PgpEncryptService {

    private final PgpFileEncryption pgpFileEncryption;
    private final EdgeConfigPath edgeConfigPath;

    @Autowired
    public PgpEncryptService(PgpFileEncryption pgpFileEncryption, EdgeConfigPath edgeConfigPath) {
        this.pgpFileEncryption = pgpFileEncryption;
        this.edgeConfigPath = edgeConfigPath;
    }


    public String gpgEncryptSign() {
        Path inputPath = Path.of("C:\\Users\\dubbril\\Desktop\\test-file\\input\\people_full_name.txt");
        Path outputPath = Path.of("C:\\Users\\dubbril\\Desktop\\test-file\\output\\people_full_name.txt.pgp");

        pgpFileEncryption.encryptPgpSign(edgeConfigPath.getEdgeDebtPublicUserId()
                , edgeConfigPath.getEdgeDebtPrivateUserId(),
                inputPath.toString(),
                outputPath.toString()
        );

        return "Success";
    }

    public String gpgDecryptVerify() {
        Path inputPath = Path.of("C:\\Users\\dubbril\\Desktop\\test-file\\input\\people_full_name.txt.gpg");
        Path outputPath = Path.of("C:\\Users\\dubbril\\Desktop\\test-file\\output\\people_full_name.txt");

        pgpFileEncryption.decryptPgpVerify(edgeConfigPath.getEdgeEdwPublicUserId(),
                inputPath.toString(),
                outputPath.toString()
        );

        return "Success";
    }
}
