package org.dubbril.learn.edge_utility.domain;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AesRequest {

  @NotBlank(message = "key is not blank")
  private String key;
  @NotBlank(message = "data is not blank")
  private String data;
}
