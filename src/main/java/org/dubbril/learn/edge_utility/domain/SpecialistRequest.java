package org.dubbril.learn.edge_utility.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Sompong Thongmee
 * @create 13/04/2023 22:58
 * @project edge-cryptography
 */
@Getter
@Setter
public class SpecialistRequest {
    private String rowNo;
    private String customerNo;
    private String customerNameTh;
    private String chkThaiName;
    private String chkEngName;
    private String customerNameEn;
    private String dob;
    private String countryCode;
    private String customerType;
    private String zipcode;
    private String action;
    private String oldAct;
    private String ovract;
    private String pob;
    private String reasonCode;
    private String rtnCustomer;
    private String srcSeq;

    public SpecialistRequest(String dataLine) {
        String[] dataFields = dataLine.split("\\|");
        this.setRowNo(dataFields[0]);
        this.setCustomerNo(dataFields[1]);
        this.setCustomerNameTh(dataFields[2]);
        this.setChkThaiName(dataFields[3]);
        this.setChkEngName(dataFields[4]);
        this.setCustomerNameEn(dataFields[5]);
        this.setDob(dataFields[6]);
        this.setCountryCode(dataFields[7]);
        this.setCustomerType(dataFields[8]);
        this.setZipcode(dataFields[9]);
        this.setAction(dataFields[10]);
        this.setOldAct(dataFields[11]);
        this.setOvract(dataFields[12]);
        this.setPob(dataFields[13]);
        this.setReasonCode(dataFields[14]);
        this.setRtnCustomer(dataFields[15]);
        this.setSrcSeq(dataFields[16]);
    }

    @Override
    public String toString() {
        return rowNo + "|" +
                customerNo + "|" +
                customerNameTh + "|" +
                chkThaiName + "|" +
                chkEngName + "|" +
                customerNameEn + "|" +
                dob + "|" +
                countryCode + "|" +
                customerType + "|" +
                zipcode + "|" +
                action + "|" +
                oldAct + "|" +
                ovract + "|" +
                pob + "|" +
                reasonCode + "|" +
                rtnCustomer + "|" +
                srcSeq;
    }
}
