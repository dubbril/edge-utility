package org.dubbril.learn.edge_utility.domain.data;

import lombok.Data;

@Data
public class PeopleFullName {
    private String fullName;
    private String nationality;

    public PeopleFullName(String csvData) {
        String[] split = csvData.split("\\|");
        this.fullName = split[0];
        this.nationality = split[1];
    }
}
