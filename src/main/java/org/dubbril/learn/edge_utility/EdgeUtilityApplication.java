package org.dubbril.learn.edge_utility;

import java.awt.Desktop;
import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication(scanBasePackages ="org.dubbril.learn.edge_utility")
public class EdgeUtilityApplication {
    public static void main(String[] args) {
        if (!isPortInUse("localhost", 8084)) {
            SpringApplication.run(EdgeUtilityApplication.class, args);
        }
        String url = "http://localhost:8084";
        openHomePage(url);
    }

    public static boolean isPortInUse(String host, int port) {
        try (Socket socket = new Socket(host, port)) {
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @SneakyThrows
    private static void openHomePage(String url) {
        if (Desktop.isDesktopSupported()) {
            Desktop desktop = Desktop.getDesktop();
            try {
                desktop.browse(new URI(url));
            } catch (IOException | URISyntaxException e) {
                log.error("Error DesktopSupport => ", e);
                throw e;
            }
        } else {
            String os = System.getProperty("os.name").toLowerCase();
            java.util.List<String> command = new ArrayList<>();
            if (os.contains("win")) {
                command.add("rundll32");
                command.add("url.dll,FileProtocolHandler");
                command.add(url);
                execCommand(command);
            } else if (os.contains("mac")) {
                try {
                    command.add("open");
                    command.add(url);
                    execCommand(command);
                } catch (IOException e) {
                    log.error("Error Run Command on macOS => ", e);
                    throw e;
                }
            } else {
                log.error("Unsupported operating system");
                throw new UnsupportedOperationException("Unsupported operating system");
            }
        }
    }

    private static void execCommand(java.util.List<String> command) throws IOException, InterruptedException {
        ProcessBuilder processBuilder = new ProcessBuilder(command);
        Process process = processBuilder.start();
        int exitCode = process.waitFor();
        log.info("Process exited with code: " + exitCode);
    }
}
