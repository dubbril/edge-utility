package org.dubbril.learn.edge_utility.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

import java.util.*;

@UtilityClass
public class JsonConvert {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static Object convertTo(Object object, JsonCase jsonCase) {
        if (object instanceof List<?>) {
            return convertJsonArray(object, jsonCase);
        }
        return convertJsonObject(object, jsonCase);
    }

    @SneakyThrows
    private static Map<String, Object> convertJsonObject(Object object, JsonCase jsonCase) {
        Map<String, Object> result = new HashMap<>();
        Map<String, Object> mapObject = objectMapper.convertValue(object, new TypeReference<>() {
        });
        for (var entryMap : mapObject.entrySet()) {
            String modifyKey = jsonCase.convert().apply(entryMap.getKey());
            result.put(modifyKey, entryMap.getValue());
            Object currentValue = entryMap.getValue();
            if (currentValue instanceof Map<?, ?>) {
                Map<String, Object> stringObjectMap = convertJsonObject(currentValue, jsonCase);
                result.put(modifyKey, stringObjectMap);
            }

            if (currentValue instanceof List<?>) {
                List<Object> maps = convertJsonArray(currentValue, jsonCase);
                result.put(modifyKey, maps);
            }
        }
        return result;
    }

    @SneakyThrows
    @SuppressWarnings("unchecked")
    private static List<Object> convertJsonArray(Object object, JsonCase jsonCase) {
        List<Object> result = new ArrayList<>(Collections.emptyList());
        ArrayList<LinkedHashMap<String, Object>> objectData = (ArrayList<LinkedHashMap<String, Object>>) object;
        for (Object dataItem : objectData) {
            if (dataItem instanceof LinkedHashMap<?, ?>) {
                Map<String, Object> stringObjectMap = convertJsonObject(dataItem, jsonCase);
                result.add(stringObjectMap);
            } else {
                result.add(dataItem);
            }
        }
        return result;
    }
}
