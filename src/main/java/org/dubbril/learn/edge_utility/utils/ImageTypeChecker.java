package org.dubbril.learn.edge_utility.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Slf4j
@UtilityClass
public class ImageTypeChecker {

    public static MediaType checkImageType(byte[] imageBytes) {
        try (InputStream inputStream = new ByteArrayInputStream(imageBytes)) {
            byte[] header = new byte[8];
            int bytesRead = inputStream.read(header);
            if (bytesRead >= 2 && header[0] == (byte) 0xFF && header[1] == (byte) 0xD8) {
                return MediaType.IMAGE_JPEG;
            } else if (bytesRead >= 8 && header[0] == (byte) 0x89 && header[1] == 'P' && header[2] == 'N' &&
                    header[3] == 'G' && header[4] == 0x0D && header[5] == 0x0A && header[6] == 0x1A && header[7] == 0x0A) {
                return MediaType.IMAGE_PNG;
            } else if (bytesRead >= 6 && header[0] == 'G' && header[1] == 'I' && header[2] == 'F' &&
                    header[3] == '8' && (header[4] == '7' || header[4] == '9') && header[5] == 'a') {
                return MediaType.IMAGE_GIF;
            } else {
                return MediaType.IMAGE_JPEG;
            }
        } catch (IOException e) {
            log.error("Error Check Image => ", e);
            return MediaType.TEXT_PLAIN;
        }
    }
}
