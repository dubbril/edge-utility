package org.dubbril.learn.edge_utility.utils;

import java.util.function.UnaryOperator;

public enum JsonCase {
    CAMEL_CASE_TO_SNAKE_CASE() {
        @Override
        public UnaryOperator<String> convert() {
            return keyName -> {
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < keyName.length(); i++) {
                    char currentChar = keyName.charAt(i);
                    if (Character.isUpperCase(currentChar)) {
                        result.append("_").append(Character.toLowerCase(currentChar));
                    } else {
                        result.append(currentChar);
                    }
                }
                return result.toString();
            };
        }
    },
    SNAKE_CASE_TO_CAMEL_CASE() {
        @Override
        public UnaryOperator<String> convert() {
            return keyName -> {
                if (!keyName.contains("_")) {
                    return keyName;
                }

                StringBuilder result = new StringBuilder();
                boolean nextUpper = false;
                for (char c : keyName.toCharArray()) {
                    if (c == '_') {
                        nextUpper = true;
                    } else if (nextUpper) {
                        result.append(Character.toUpperCase(c));
                        nextUpper = false;
                    } else {
                        result.append(Character.toLowerCase(c));
                    }
                }
                return result.toString();
            };
        }
    };

    public abstract UnaryOperator<String> convert();
}
