package org.dubbril.learn.edge_utility.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.SneakyThrows;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("java:S1874")
public final class ModelConverter {

  /** Normal object mapper. */
  private static final ObjectMapper OBJECT_MAPPER;
  /** Object mapper with lower case property name. */
  private static final ObjectMapper OBJECT_MAPPER_LOWER;

  static {
    OBJECT_MAPPER = new ObjectMapper();
    OBJECT_MAPPER.registerModule(new JavaTimeModule());
    OBJECT_MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    OBJECT_MAPPER.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    OBJECT_MAPPER_LOWER = new ObjectMapper();
    OBJECT_MAPPER_LOWER.setPropertyNamingStrategy(new MapperNamingLowerCaseStrategy());
    OBJECT_MAPPER_LOWER.registerModule(new JavaTimeModule());
    OBJECT_MAPPER_LOWER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    OBJECT_MAPPER_LOWER.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
  }

  private ModelConverter() {}

  public static <T> Map<String, Object> modelToMap(T object) {
    return modelToMap(object, false);
  }

  public static <T> Map<String, Object> modelToMap(T object, boolean isKeyLowerCase) {
    Map<String, Object> convertModel = OBJECT_MAPPER.convertValue(object, Map.class);
    if (isKeyLowerCase) {
      Map<String, Object> lowerCaseMap = new HashMap<>();
      for (Map.Entry<String, Object> entry : convertModel.entrySet()) {
        lowerCaseMap.put(entry.getKey().toLowerCase(), entry.getValue());
      }
      return lowerCaseMap;
    }
    return convertModel;
  }

  public static <T> JSONObject modelToJsonObject(T object)
      throws JsonProcessingException, JSONException {
    return modelToJsonObject(object, false);
  }

  public static JSONArray modelToJsonArray(Object obj)
      throws JsonProcessingException, JSONException {
    return new JSONArray(modelToJsonString(obj, false));
  }

  public static <T> JSONObject modelToJsonObject(T object, boolean isKeyLowerCase)
      throws JsonProcessingException, JSONException {
    return new JSONObject(modelToJsonString(object, isKeyLowerCase));
  }

  public static <T> T jsonStringToModel(String json, Class<T> type) throws JsonProcessingException {
    return OBJECT_MAPPER.readValue(json, type);
  }

  public static <T> T jsonToModel(JSONObject object, Class<T> type) throws JsonProcessingException {
    return OBJECT_MAPPER.readValue(object.toString(), type);
  }

  public static Map<String, Object> jsonToMap(JSONObject object) throws JsonProcessingException {
    return OBJECT_MAPPER.readValue(object.toString(), HashMap.class);
  }

  private static String modelToJsonString(Object obj, boolean isKeyLowerCase)
      throws JsonProcessingException {
    String jsonString;
    if (isKeyLowerCase) {
      jsonString = OBJECT_MAPPER_LOWER.writeValueAsString(obj);
    } else {
      jsonString = OBJECT_MAPPER.writeValueAsString(obj);
    }
    return jsonString;
  }

  @SneakyThrows
  public static String modelToString(Object obj){
    return OBJECT_MAPPER.writeValueAsString(obj);
  }

  public static class MapperNamingLowerCaseStrategy extends PropertyNamingStrategy {
    @Override
    public String nameForField(MapperConfig<?> config, AnnotatedField field, String defaultName) {
      return convert(defaultName);
    }

    @Override
    public String nameForGetterMethod(
        MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
      return convert(defaultName);
    }

    @Override
    public String nameForSetterMethod(
        MapperConfig<?> config, AnnotatedMethod method, String defaultName) {
      return convert(defaultName);
    }

    private String convert(String input) {
      return input.toLowerCase();
    }
  }
}
