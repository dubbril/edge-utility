package org.dubbril.learn.edge_utility.utils;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;

@Service
@SuppressWarnings("java:S5542")
public class AESEncrypt {

  private static final String ALGORITHM = "AES";
  private static final String CIPHER = "AES";


  public String encryptWithHexKey(String text, String key) throws DecoderException, GeneralSecurityException {
    SecretKey secretKey = new SecretKeySpec(Hex.decodeHex(key), ALGORITHM);

    Cipher cipher = Cipher.getInstance(CIPHER);
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);

    byte[] encrypted = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));
    return Hex.encodeHexString(encrypted);
  }

  public String encryptWithBase64Key(String text, String key) throws GeneralSecurityException {

    SecretKey secretKey = new SecretKeySpec(Base64.decodeBase64(key), ALGORITHM);

    Cipher cipher = Cipher.getInstance(CIPHER);
    cipher.init(Cipher.ENCRYPT_MODE, secretKey);

    byte[] encrypted = cipher.doFinal(text.getBytes(StandardCharsets.UTF_8));
    return Base64.encodeBase64String(encrypted);
  }


  public String decryptWithHexKey(String encrypted, String key) throws DecoderException, GeneralSecurityException {

    SecretKey secretKey = new SecretKeySpec(Hex.decodeHex(key), ALGORITHM);
    byte[] decoded = Hex.decodeHex(encrypted);

    Cipher cipher = Cipher.getInstance(CIPHER);
    cipher.init(Cipher.DECRYPT_MODE, secretKey);

    byte[] cipherText = cipher.doFinal(decoded);
    return new String(cipherText, StandardCharsets.UTF_8);
  }

  public String decryptWithBase64Key(String encrypted, String key) throws GeneralSecurityException {
    SecretKey secretKey = new SecretKeySpec(Base64.decodeBase64(key), ALGORITHM);
    byte[] decoded = Base64.decodeBase64(encrypted);

    Cipher cipher = Cipher.getInstance(CIPHER);
    cipher.init(Cipher.DECRYPT_MODE, secretKey);

    byte[] cipherText = cipher.doFinal(decoded);
    return new String(cipherText, StandardCharsets.UTF_8);
  }

}
