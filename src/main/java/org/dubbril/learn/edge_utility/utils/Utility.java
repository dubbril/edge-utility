package org.dubbril.learn.edge_utility.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

@Slf4j
@UtilityClass
public class Utility {

    private static final Pattern hexPattern = Pattern.compile("^[0-9a-fA-F]+$");
    Map<String, String> idCardMap = new HashMap<>();
    Random random = new Random();

    public static boolean isHexString(String str) {
        return hexPattern.matcher(str).matches();
    }

    public String dateWithYearMonthDay() {
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        return date.format(formatter);
    }

    public static String generateRandomThaiIDCard() {


        StringBuilder idCard = new StringBuilder();
        idCard.append(random.nextInt(9) + 1);

        // Generate the first 11 digits
        for (int i = 0; i < 11; i++) {
            idCard.append(random.nextInt(10)); // Append random digits (0-9)
        }

        // Generate check digit (last digit)
        int checkDigit = calculateCheckDigit(idCard.toString());
        idCard.append(checkDigit);

        String genIdCard = idCard.toString();

        if (idCardMap.containsKey(genIdCard)) {
            generateRandomThaiIDCard();
            log.info("Duplicate : {}", genIdCard);
        } else {
            idCardMap.put(genIdCard, genIdCard);
        }
        return genIdCard;
    }

    private static int calculateCheckDigit(String idWithoutCheckDigit) {
        int sum = 0;
        for (int i = 0; i < idWithoutCheckDigit.length(); i++) {
            int digit = Character.getNumericValue(idWithoutCheckDigit.charAt(i));
            sum += digit * (13 - i);
        }
        return (11 - (sum % 11)) % 10;
    }
}
