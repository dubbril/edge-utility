package org.dubbril.learn.edge_utility.exception;

public class CustomException extends RuntimeException {

  public CustomException(String message) {
    super(message);
  }

  public CustomException(Throwable t) {
    super(t);
  }
}
